class AddGetEmailWhenCommentMadeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :email_when_comment_made, :boolean, :default => false
  end
end
