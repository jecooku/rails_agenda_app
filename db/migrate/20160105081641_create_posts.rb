class CreatePosts < ActiveRecord::Migration
  def up
    create_table :posts do |t|
      t.integer :user_id, :null => false #foreign key
      t.integer :position
      t.string :content_type, :default => "text"
      t.text :content
      t.timestamps null: false
    end
  end
  
  def down
    drop_table :posts
  end
end
