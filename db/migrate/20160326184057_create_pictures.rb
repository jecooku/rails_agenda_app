class CreatePictures < ActiveRecord::Migration
  def up
    create_table :pictures do |t|
      t.attachment :image
    end
  end
  
  def down
    remove_attachment :pictures, :image
  end
end
