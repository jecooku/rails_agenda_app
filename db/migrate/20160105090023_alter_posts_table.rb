class AlterPostsTable < ActiveRecord::Migration
  def up
    add_index("posts", "user_id")
    add_index("posts", "position")
  end
  
  def down
    remove_index("posts", "position")
    remove_index("posts", "user_id")
  end
end
