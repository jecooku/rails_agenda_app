class AddGetEmailWhenFollowingUserPostsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :email_when_following_posts, :boolean, :default => false
  end
end
