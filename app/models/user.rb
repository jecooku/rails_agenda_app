class User < ActiveRecord::Base
  
  has_many :posts

  has_many :follows, foreign_key: 'follower_id'
  has_many :followed_users, through: :follows
  has_many :inverse_follows, :class_name => "Follow", foreign_key: 'followed_user_id'
  has_many :followers, through: :inverse_follows, source: :follower
  
  has_attached_file :avatar, styles: { large: "600x600>", medium: "300x300>", small: "150x150>", xsmall: "100x100#", thumb: "50x50#", mobile: "80x80>"}, 
      :default_url => "missing.jpg"
      
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  validates_uniqueness_of :username
end
