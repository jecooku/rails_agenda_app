class Post < ActiveRecord::Base
  
  belongs_to :user
  has_many :comments
  has_many :pictures, :dependent => :destroy
  after_destroy :destroy_comments
  
  def destroy_comments
    self.comments.destroy_all
  end
  
end
