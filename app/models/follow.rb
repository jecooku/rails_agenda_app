class Follow < ActiveRecord::Base
  
  #validates :name, :presence => true, :uniqueness => true
  validates_uniqueness_of :follower_id, :scope => :followed_user_id
  
  belongs_to :follower, class_name: 'User', foreign_key: 'follower_id'
  belongs_to :followed_user, class_name: 'User', foreign_key: 'followed_user_id'
  has_many :followed_posts, through: :followed_user, source: :posts
 
end
