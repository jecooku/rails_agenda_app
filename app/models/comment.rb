class Comment < ActiveRecord::Base
  
  belongs_to :post
  
  def create_send_and_save(user, post)
    if self.save
      if !User.where(:id => post.user_id).blank? && (User.find_by id: post.user_id).email_when_comment_made?
        CommentMailer.comment_created(user, post.user, self.content).deliver
      end
    end
  end
  
  
end