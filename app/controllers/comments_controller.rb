class CommentsController < ApplicationController
  
  before_action :find_post
  before_action :find_subject
  
  def create
    @comment = @post.comments.new(params.require(:comment).permit(:content, :author))
    if @comment.create_send_and_save(@user, @post)
      flash[:notice] = "Your comment has been saved"
      respond_to do |format|
        format.html { redirect_to(post_path(@post)) }
        format.js 
      end
    else
      flash[:notice] = "Your comment was not saved"
    end
  end
  
  private
  
  def find_post
    if params[:post_id]
      @post = Post.find(params[:post_id])
    end
  end
  
end
