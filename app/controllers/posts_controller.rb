class PostsController < ApplicationController
  
  before_action :authenticate_user!, :except => [:index, :show]
  before_action :find_subject
  
  layout "public"
  
  def index
    @posts = Post.order("created_at DESC").page(params[:page]).per_page(6)
  end

  def show
    @post = Post.find(params[:id])
    
    if user_signed_in? && current_user.followed_users.where(:id => @post.user_id).blank?
      @subscribed = false
    end
  end

  def new
    @post = @user.posts.new(:author => @user.username)
  end
  
  def create
    @post = @user.posts.new(post_params)
    if @post.save
      if params[:images]
        params[:images].each do |im|
          @post.pictures.create(:image => im)
        end
      end
      flash[:notice] = "Post created successfully"
      send_email_to_followers(@user)
      redirect_to(:action => :show, :id => @post.id)
    else
      render("new")
    end
  end

  def edit
    @post = Post.find(params[:id])
  end
  
  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      flash[:notice] = "Post updated successfully"
      redirect_to(:action => :show)
    else
      render("edit")
    end
  end

  def delete
    @post = Post.find(params[:id])
  end
  
  def destroy
    post = Post.find(params[:id])
    post.destroy
    flash[:notice] = "Post '#{post.title}' destroyed successfully"
    redirect_to(:action => :index)
  end
  
  private
  
  def post_params
    params.require(:post).permit(:title, :content, :content_type, :author, :pictures)
  end
  
  def logged_id?
    user_signed_in?
  end
  
end
