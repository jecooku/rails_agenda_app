class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_action :configure_permitted_parameters, if: :devise_controller?

  layout :layout_by_resource
  protected
  
  def layout_by_resource
    if devise_controller?
      "public"
    else
      "application"
    end
  end
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :first_name, :last_name,  :password, :password_confirmation, :remember_me, :avatar) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :last_name, :first_name, :email, :password, :password_confirmation, :current_password, :avatar) }
  end
  
  def find_subject
    if user_signed_in?
      @user = current_user
    end
  end
  
  def send_email_to_followers(user)
    @destinatarios = user.followers
    @destinatarios.each do |destinatario|
      if destinatario.email_when_following_posts?
        PostMailer.post_created(destinatario).deliver
      end
    end
  end
  
  def send_email_to_followed_user(post)
    @destinatario = User.find(post.user_id) #Post's author
    PostMailer.post_created(@destinatario).deliver
  end
  
  def send_email_to_comment_author(user, post_author, content)
    CommentMailer.comment_created(user, post_author, content).deliver
  end

end
