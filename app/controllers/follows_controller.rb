class FollowsController < ApplicationController
  
  layout "public"
  
  before_action :authenticate_user!
  
  def index
    @follow = @user.follows.find_by(params[:followed_user_id])
  end
  
  def create
    @user = User.find(params[:follower_id])
    @follow = Follow.new(params.permit(:follower_id, :followed_user_id))
    @post = Post.find(params[:post_id])
    if @follow.save
      followed_user = User.find(params[:followed_user_id])
      flash[:notice] = "You now follow '#{followed_user.username}'"
      respond_to do |format|
        format.html { redirect_to :back }
        format.js
      end
    end
  end
  
  def destroy
    @user = User.find(params[:follower_id])
    if !params[:post_id].blank?
      @post = Post.find(params[:post_id])
    end
    @follow = @user.follows.find_by(params[:followed_user_id])
    @follow.destroy
    respond_to do |format|
        format.html { redirect_to :back }
        format.js
    end
  end
  
  def followers
    @user = User.find(params[:id])
    @followers = @user.followers.order("username ASC")
  end
  
  def following
    @user = User.find(params[:id])
    @following = @user.followed_users.order("username ASC")
  end
  
end
