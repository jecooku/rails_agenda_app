class UsersController < ApplicationController
  
  layout "public"
  
  before_action :authenticate_user!
  before_action :verify_is_admin, :except => [:show, :notifications]
  
  def index
    @users = User.order("last_name ASC")
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def notifications
    @user = User.find(params[:id])
    restrict_access if @user.id != current_user.id
  end
  
  def notifications_edit
    @user = User.find(params[:id])
    if @user.update_attributes(params.require(:user).permit(:email_when_comment_made, 
      :email_when_following_posts))
      flash[:notice] = "User updated successfully"
      redirect_to root_path
    else
      flash[:notice] = "Error, User not updated successfully"
      render("notifications")
    end
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:notice] = "User updated successfully"
      redirect_to root_path
    else
      flash[:notice] = "Error, User not updated successfully"
      render("edit")
    end
  end
  
  def delete
    @user = User.find(params[:id])
  end
  
  def destroy
    @user = User.find(params[:id])
    @follows = @user.follows
    @Inverse_follows = @user.inverse_follows
    @follows.destroy_all
    @Inverse_follows.destroy_all
    if @user.destroy
      flash[:notice] = "User deleted successfully"
      redirect_to root_path
    end
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = "User added successfully"
      redirect_to(users_path)
    else
      flash[:notice] = "Error. User not added"
      render("new")
    end
  end

  def verify_is_admin
    (current_user.nil?) ? redirect_to(root_path) : (redirect_to(root_path) unless current_user.admin?)
  end
  
  def show
    @user = User.find(params[:id])
    @posts = @user.posts.order("created_at DESC")
  end
  
  private
  
  def user_params
    params.require(:user).permit(:first_name, 
      :last_name, :username, :email, :password, :password_confirmation, :admin, :avatar)
  end
  
  def restrict_access
    redirect_to root_path 
    flash[:notice] = "Access denied"
  end
  
end
