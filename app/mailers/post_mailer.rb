class PostMailer < ActionMailer::Base
  
  #each method is a mail I want to send
  def post_created(user)
    mail({to: user.email,
          from: "Jorge's post app",
          subject: "New post created",
          body: "a new Post has been created"})
  end
  
end