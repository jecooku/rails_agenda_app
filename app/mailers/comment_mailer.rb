class CommentMailer < ActionMailer::Base
  
  #each method is a mail I want to send
  def comment_created(user, post_author, content)
    
    unless @user.blank?
      @user = user.username;
    end
    @post_author = post_author.username;
    @content = content;
    
    mail({to: post_author.email,
          from: "Jorge's post app",
          subject: "Someone commented"})
  end
  
end