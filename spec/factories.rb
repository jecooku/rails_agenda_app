FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    username "elnuevonombreficticio"
    email { Faker::Internet.email }
    password { Faker::Internet.password(8) } 
    admin true
  end
  
  factory :post do
    author = "author"
    title = "title"
    position = 0
    content = "ogi"
  end
end

