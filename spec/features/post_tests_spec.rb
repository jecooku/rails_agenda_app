require 'rails_helper'

RSpec.describe "PostTests" do
  
  let(:user) { FactoryGirl::create(:user)}
  
  it "creates a new post" do
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Create new post"
    expect(page).to have_content 'Create Post'
    post = build(:post)
    click_button "Create post"
    expect(page).to have_content 'Post created successfully '
  end
  
  it "Show post" do
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Create new post"
    expect(page).to have_content 'Create Post'
    post = build(:post)
    click_button "Create post"
    expect(page).to have_content 'Post created successfully '
    click_link "Home"
    first(:link, "Show Post").click
    expect(page).to have_content 'Post Deatils'
  end
  
  it "Delete post" do
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Create new post"
    expect(page).to have_content 'Create Post'
    post = build(:post)
    click_button "Create post"
    expect(page).to have_content 'Post created successfully '
    click_link "Home"
    first(:link, "Delete").click
    expect(page).to have_content 'Delete a post'
    click_button "Delete Post"
    expect(page).to have_content 'destroyed successfully'
  end
  
  it "Edit post" do
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Create new post"
    expect(page).to have_content 'Create Post'
    post = build(:post)
    click_button "Create post"
    expect(page).to have_content 'Post created successfully '
    click_link "Home"
    first(:link, "Edit").click
    expect(page).to have_content 'Edit Post'
    post = build(:post, :title => "new title")
    click_button "Update post"
    expect(page).to have_content 'Post updated successfully '
  end
end
