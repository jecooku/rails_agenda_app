require 'rails_helper'
require 'spec_helper'

RSpec.describe "UserManages" do
 
  let(:user) { FactoryGirl::create(:user)}
  
  it "Edit user" do
    #user = build(:user, :email => "dummy@example.com", :password => 12345678)
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Manage account"
    expect(page).to have_content 'Edit User'
    fill_in "user[current_password]", :with => user.password
    click_button "Update"
    expect(page).to have_content 'Your account has been updated successfully'
  end
  
  it "Access admin section" do
    #user = build(:user, :email => "dummy@example.com", :password => 12345678)
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Admin Section"
    expect(page).to have_content 'Admin Index'
  end
  
  it "Edit user" do
    #user = build(:user, :email => "dummy@example.com", :password => 12345678)
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Admin Section"
    expect(page).to have_content 'Admin Index'
    first(:link, "Edit user").click
    expect(page).to have_content 'Edit user'
    fill_in "user[password]", :with => user.password
    fill_in "user[password_confirmation]", :with => user.password
    click_button "Update user"
    expect(page).to have_content 'User updated successfully '
  end
  
  it "Add user" do
    #user = build(:user, :email => "dummy@example.com", :password => 12345678)
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Admin Section"
    expect(page).to have_content 'Admin Index'
    first(:link, "Add user").click
    expect(page).to have_content 'Create User'
    user = build(:user)
    fill_in "user[first_name]", :with => user.first_name
    fill_in "user[last_name]", :with => user.last_name
    fill_in "user[username]", :with => user.username
    fill_in "user[email]", :with => user.email
    fill_in "user[password]", :with => user.password
    fill_in "user[password_confirmation]", :with => user.password
    check 'user[admin]'
    click_button "Add user"
    expect(page).to have_content 'Hello, ' + user.username
  end
  
  it "Delete user" do
    #user = build(:user, :email => "dummy@example.com", :password => 12345678)
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Admin Section"
    expect(page).to have_content 'Admin Index'
    first(:link, "Add user").click
    expect(page).to have_content 'Create User'
    user = build(:user)
    fill_in "user[first_name]", :with => user.first_name
    fill_in "user[last_name]", :with => user.last_name
    fill_in "user[username]", :with => user.username
    fill_in "user[email]", :with => user.email
    fill_in "user[password]", :with => user.password
    fill_in "user[password_confirmation]", :with => user.password
    check 'user[admin]'
    click_button "Add user"
    expect(page).to have_content 'Hello, ' + user.username
    click_link "Admin Section"
    expect(page).to have_content 'Admin Index'
    first(:link, "Delete user").click
    expect(page).to have_content 'Delete User'
    click_button "Delete user"
    expect(page).to have_content 'User deleted successfully'
  end
end
