require 'rails_helper'

RSpec.describe "UserLogins" do
  
  let(:user) { FactoryGirl::create(:user)}
  
  it "displays message to the user indicating errors" do
    user = build(:user)
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Invalid'
  end
  
  it "logs in user when correct information is entered" do
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    #click_link "Sign out"
  end
end




