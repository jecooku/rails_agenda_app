require 'rails_helper'

RSpec.describe "UserLogouts" do
  
  let(:user) { FactoryGirl::create(:user)}
  
  it "logs out user" do
    visit new_user_session_path
    fill_in "user[password]", :with => user.password
    fill_in "user[email]", :with => user.email
    click_button "Log in"
    expect(page).to have_content 'Signed in successfully'
    click_link "Sign out"
    expect(page).to have_content 'Signed out successfully. '
  end
  
end
